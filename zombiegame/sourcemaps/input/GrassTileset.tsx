<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Tileset" tilewidth="64" tileheight="64">
 <image source="tileset-rural.png" width="512" height="1280"/>
 <tile id="128">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="129">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="130">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="131">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="136">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="137">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="138">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="139">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="146">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="147">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="152">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="153">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="154">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
 <tile id="155">
  <properties>
   <property name="Terrain" value="grass"/>
  </properties>
 </tile>
</tileset>
