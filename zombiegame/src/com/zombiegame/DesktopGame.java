package com.zombiegame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker;
import com.badlogic.gdx.tools.imagepacker.TexturePacker.Settings;
import com.zombiegame.util.DummyGameReporter;

public class DesktopGame {

	public static void main(String[] args) {
		Settings settings = new Settings();

		TexturePacker.process(settings, "images/game", "data/gfx/game");
		TexturePacker.process(settings, "images/menu", "data/gfx/menu");

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = ZombieGame.HEIGHT;
		config.width = ZombieGame.WIDTH;
		config.useGL20 = false;
		config.title = "ZombieGame";
		config.fullscreen = false;

		new LwjglApplication(new ZombieGame(new DummyGameReporter()), config);
	}
}
