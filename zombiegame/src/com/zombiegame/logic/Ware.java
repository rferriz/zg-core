package com.zombiegame.logic;

public class Ware {
	public static boolean checkValid(int ware) {
		boolean valid = true;

		if (ware < Min || ware >= Size)
			valid = false;

		if (!valid)
			throw new IllegalArgumentException("Invalid ware index: " + ware);

		return valid;
	}

	public static int pkgSize(int ware) {
		checkValid(ware);
		switch (ware) {
		case none:
			return 0;
		case survivor:
			return 1;
		default:
			return 10;
		}
	}

	public static int valueOf(String str) {
		int result = 0;

		for (int i = Min; i < Size; i++) {
			if (str.equals(name[i]))
				result = i;
		}

		return result;
	}

	public static String[] name = { "none", "survivor", "wood", "bullet" };

	public static final int Min = 0;
	public static final int none = Min;
	public static final int survivor = none + 1;
	public static final int wood = survivor + 1;
	public static final int bullet = wood + 1;
	public static final int Size = bullet + 1;
}
