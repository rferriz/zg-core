package com.zombiegame.logic;

import java.util.ArrayList;

import com.badlogic.gdx.utils.Disposable;
import com.zombiegame.logic.gameobject.GameObjectMapResource;

public class MapResourceManager implements Disposable {
	ArrayList<GameObjectMapResource> tiles;

	public MapResourceManager() {
		init();
	}

	public void init() {
		tiles = new ArrayList<GameObjectMapResource>();
	}

	@Override
	public void dispose() {
		tiles.clear();
		tiles = null;
	}

	public void reset() {
		if (tiles == null)
			init();
		tiles.clear();
	}

	public int resourceCount(int tile) {
		for (int i = 0; i < tiles.size(); i++) {
			GameObjectMapResource mrt = tiles.get(i);
			if (mrt.getTile() == tile)
				return mrt.resourceCount();
		}
		return 0;
	}

	public int resourceCount(int tile, int key) {
		for (int i = 0; i < tiles.size(); i++) {
			GameObjectMapResource mrt = tiles.get(i);
			if (mrt.getTile() == tile)
				return mrt.resourceCount(key);
		}
		return 0;
	}

	public int randomResource(GameWorld world, int tile) {
		for (int i = 0; i < tiles.size(); i++) {
			GameObjectMapResource mrt = tiles.get(i);
			if (mrt.getTile() == tile)
				return mrt.randomResource(world);
		}
		return Ware.none;
	}

	public void setResource(int tile, int i2, int q) {

		GameObjectMapResource mrt;

		for (int i = 0; i < tiles.size(); i++) {
			mrt = tiles.get(i);
			if (mrt.getTile() == tile) {
				mrt.setResource(i2, q);
				return;
			}
		}

		mrt = new GameObjectMapResource();
		mrt.setTile(tile);
		mrt.setResource(i2, q);
		tiles.add(mrt);
	}

	public void extractResource(int tile, int ware) {
		for (int i = 0; i < tiles.size(); i++) {
			GameObjectMapResource mrt = tiles.get(i);
			if (mrt.getTile() == tile) {
				if (mrt.extractResource(ware))
					return;
			}
		}
		throw new IllegalArgumentException("tile do not have any resource");

	}

	public boolean hasZombies(int tile) {
		for (int i = 0; i < tiles.size(); i++) {
			GameObjectMapResource mrt = tiles.get(i);
			if (mrt.getTile() == tile && mrt.hasZombies())
				return true;
		}
		return false;
	}

}
