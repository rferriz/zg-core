package com.zombiegame.logic;

import java.util.ArrayList;
import java.util.List;

import ai.GridPathfinder.GridMap;
import ai.GridPathfinder.GridMover;
import ai.GridPathfinder.GridNode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;
import com.zombiegame.logic.gameobject.GameObjectZombie;

public class GameTiledMap implements GridMap {

	private static final String TAG = "GameTiledMap";

	public static final int INVALID = 0;
	public static final int WATER = INVALID + 1;
	public static final int GRASS = WATER + 1;
	public static final int TREE = GRASS + 1;
	public static final int WALL = TREE + 1;

	/* Tiled map */
	TiledMap tiledMap;

	/* The terrain settings for each tile in the map */
	int[][] terrain;
	/* The unit of each tile in the map */
	List<GameObjectOwned>[][] units;
	/* The building of each tile in the map */
	GameObjectOwned[][] buildings;

	GridNode[][] nodes;

	/* Temporal, for cache */
	GridNode target;

	public GameTiledMap() {
		target = new GridNode();
	}

	@SuppressWarnings("unchecked")
	public void setTiledMap(final TiledMap map) {
		tiledMap = map;
		terrain = new int[tiledMap.width][tiledMap.height];
		units = (List<GameObjectOwned>[][]) new ArrayList[tiledMap.width][tiledMap.height];
		buildings = new GameObjectOwned[tiledMap.width][tiledMap.height];

		Gdx.app.log(TAG, tiledMap.width + "x" + tiledMap.height);
		nodes = new GridNode[tiledMap.width][tiledMap.height];
		for (int row = 0; row < tiledMap.height; row++)
			for (int col = 0; col < tiledMap.width; col++) {
				terrain[col][row] = getTerrainType(col, row);
				units[col][row] = new ArrayList<GameObjectOwned>();
				nodes[col][row] = new GridNode(col, row);
			}
	}

	public TiledMap getTiled() {
		return tiledMap;
	}

	@Override
	public int getWidth() {
		return tiledMap.width;
	}

	@Override
	public int getHeight() {
		return tiledMap.height;
	}

	@Override
	public boolean isBlocked(GridMover mover, int x, int y) {

		// TODO If there is a unit at the position, then it's blocked
		List<GameObjectOwned> lgo = getUnits(x, y);
		if (mover instanceof GameObjectSurvivor) {
			for (int i = 0; i < lgo.size(); i++) {
				GameObjectOwned go = lgo.get(i);
				if (go instanceof GameObjectZombie && go.isAlive())
					return true;
			}
		}

		// if (getUnit(x, y) != null) {
		// return true;
		// }

		if (terrain[x][y] == GRASS) {
			return false;
		}

		/* Unknown mover, so don't allow */
		return true;
	}

	@Override
	public float getCost(GridMover mover, GridNode from, GridNode to) {
		final float SQUARE_ROOT_OF_2 = 1.414213562373f;
		// Buildings are avoided
		if (getBuilding(to.getX(), to.getY()) != null) {
			mover.getEndNode(target);
			if (target.getX() != to.getX() || target.getY() != to.getY())
				return 20f;
		}

		int len2 = Math.abs(to.getX() - from.getX())
				+ Math.abs(to.getY() - from.getY());
		if (len2 == 1)
			return 1f;
		return SQUARE_ROOT_OF_2;
	}

	public GameObjectOwned getBuilding(int x, int y) {
		return buildings[x][y];
	}

	public void setBuilding(GameObjectOwned o, int x, int y) {
		buildings[x][y] = o;
	}

	public List<GameObjectOwned> getUnits(int x, int y) {
		return units[x][y];
	}

	public void addUnit(GameObjectOwned o, int x, int y) {
		if (!units[x][y].contains(o))
			units[x][y].add(o);
	}

	public void removeUnit(GameObjectOwned o, int x, int y) {
		units[x][y].remove(o);
	}

	public void setTerrain(int terrain, int x, int y) {
		this.terrain[x][y] = terrain;
	}

	public int getTerrainType(int x, int y) {
		int gid, terrainType = INVALID;
		String terrainString;

		for (int index = tiledMap.layers.size() - 1; index >= 0; index--) {

			gid = tiledMap.layers.get(index).tiles[y][x];
			terrainString = tiledMap.getTileProperty(gid, "Terrain");

			if (terrainString != null) {
				if ("water".equals(terrainString.toLowerCase())) {
					terrainType = WATER;
				} else if ("grass".equals(terrainString.toLowerCase())) {
					terrainType = GRASS;
				} else if ("tree".equals(terrainString.toLowerCase())) {
					terrainType = TREE;
				} else if ("wall".equals(terrainString.toLowerCase())) {
					terrainType = WALL;
				} else {
					Gdx.app.log(TAG, "Unknown terrain string " + terrainString);
				}

				if (terrainType != INVALID)
					break;
			}
		}

		return terrainType;
	}

	@Override
	public GridNode getNode(int x, int y) {
		return nodes[x][y];
	}

}
