package com.zombiegame.logic;

import java.util.HashMap;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public class PropertyContainerManager {

	HashMap<Integer, Property> gameObjects;

	public PropertyContainerManager() {
		gameObjects = new HashMap<Integer, Property>();
	}

	public boolean setProperties(int pBuildingId, Property pProperties) {
		synchronized (gameObjects) {
			gameObjects.put(pBuildingId, pProperties);
			return true;
		}
	}

	public void clear() {
		gameObjects.clear();
	}

	public Property objectProperties(GameObjectOwned gameObject) {
		Property prop = null;

		if (gameObjects.containsKey(gameObject.getName()))
			prop = gameObjects.get(gameObject.getName());

		if (prop == null) {
			throw new RuntimeException("Properties not found for " + gameObject
					+ " : " + gameObject.getName());
		}
		return prop;
	}
}
