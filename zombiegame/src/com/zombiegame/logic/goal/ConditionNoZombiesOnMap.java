package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;

public class ConditionNoZombiesOnMap extends ConditionBase {

	public ConditionNoZombiesOnMap() {
	}

	@Override
	void onCheck(GameWorld world, float delta) {
		/* Simple, count zombies on map */
		int count = world.getZombies().size();

		/* TODO Add zombies in buildings */

		completed = count == 0;
	}

}
