package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectZombie;

public class ConditionKillZombies extends ConditionBase {

	int needed;
	int killed;

	public ConditionKillZombies() {
		super();
	}

	public ConditionKillZombies(int number) {
		super();
		needed = number;
	}

	@Override
	void onCheck(GameWorld world, float delta) {
		if (killed >= needed)
			completed = true;
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		if (object instanceof GameObjectZombie)
			killed++;
	}

}
