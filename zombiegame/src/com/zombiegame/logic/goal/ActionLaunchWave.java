package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;

public class ActionLaunchWave extends Action {

	public ActionLaunchWave() {
	}

	@Override
	public void onExecute(GameWorld world) {
		world.launchWave();
	}
}
