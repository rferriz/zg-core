package com.zombiegame.logic.goal;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.GameWorld;

public class ConditionComposite extends ConditionBase {

	ConditionBase first;
	ConditionBase second;
	Operation op;

	public void setOperation(Operation op) {
		this.op = op;
	}

	public void setFirst(ConditionBase condition) {
		first = condition;
	}

	public void setSecond(ConditionBase condition) {
		second = condition;
	}

	@Override
	void onCheck(GameWorld world, float delta) {
		first.check(world, delta);
		second.check(world, delta);

		switch (op) {
		case opAnd:
			completed = first.isCompleted() && second.isCompleted();
			break;
		case opOr:
			completed = first.isCompleted() || second.isCompleted();
			break;
		default:
			Gdx.app.log("CompositeCondition", "Invalid operator");
			break;
		}

	}

	public enum Operation {
		opAnd, opOr
	}

}
