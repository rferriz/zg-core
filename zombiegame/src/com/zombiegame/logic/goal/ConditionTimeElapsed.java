package com.zombiegame.logic.goal;

import com.zombiegame.logic.GameWorld;

public class ConditionTimeElapsed extends ConditionBase {

	float elapse;
	float accumulatedTime;
	boolean fired;
	boolean loop;

	public ConditionTimeElapsed() {
		accumulatedTime = 0f;
	}

	public ConditionTimeElapsed setup(float elapse, boolean loop) {
		this.elapse = elapse;
		this.loop = loop;
		return this;
	}

	@Override
	void onCheck(GameWorld world, float delta) {
		accumulatedTime += checkDelay;

		completed = false;

		/* If no loop, timer is only executed once */
		if (!fired || (fired && loop))
			completed = accumulatedTime >= elapse;

		/* Only fire once */
		if (completed) {
			fired = true;
			if (loop)
				accumulatedTime = 0;
		}
	}
}
