package com.zombiegame.logic.gameobject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Property;
import com.zombiegame.logic.ai.DummyState;
import com.zombiegame.logic.ai.IGameObjectState;
import com.zombiegame.logic.events.GameObjectListener;
import com.zombiegame.logic.events.GameObjectNotifier;

public abstract class GameObjectOwned extends GameObject implements
		GameObjectListener {

	// ========================================================================
	// Constants
	// ========================================================================

	private static final String TAG = "GameObjectOwned";

	// ========================================================================
	// Fields
	// ========================================================================

	String name;
	private boolean checkAI;
	private float delayAI;
	int damage;
	private int mTile;

	GameObjectNotifier notifier;

	private boolean dirty;

	public IGameObjectState currentState;
	public Object dataState;
	public GameObjectOwned target; // Generic GameObject target, it is used by

	// on
	// transport state

	// ========================================================================
	// Constructor
	// ========================================================================

	public GameObjectOwned() {
		super();
		GameObjectInit();
	}

	private void GameObjectInit() {
		size = new Vector2(GameWorld.$().getMap().getTiled().tileWidth,
				GameWorld.$().getMap().getTiled().tileHeight);
		notifier = new GameObjectNotifier();
		currentState = DummyState.Dummy;
	}

	@Override
	public void reset() {
		super.reset();
		notifier.notifyRemoved(this);
		notifier.removeAll();
		checkAI = false;
		delayAI = 0.0f;
		damage = 0;
		mTile = 0;
		currentState = DummyState.Dummy;
	}

	public void dispose() {
		reset();
		notifier = null;
	}

	public GameObjectNotifier getNotifier() {
		return notifier;
	}

	public int getTile() {
		if (dirty) {
			dirty = false;
			mTile = GameWorld.$().worldToTile(position.x, position.y);
		}
		return mTile;
	}

	public String getName() {
		return name;
	}

	public boolean hasTarget() {
		return target != null;
	}

	public void hurt() {
		Gdx.app.log(TAG, this + ".hurt()");

		if (isAlive())
			damage++;

	}

	public boolean isAlive() {
		return (damage < properties().getMaxHitPoints());
	}

	@Override
	public void notifyRemoved(GameObjectOwned go) {
		if (target == go) {
			target.getNotifier().removeListener(this);
			target = null;
		}
	}

	public Property properties() {
		final Property prop = GameWorld.$().getProperties(this);
		return prop;
	}

	@Override
	public void setPosition(float x, float y) {
		dirty = true;
		super.setPosition(x, y);
	}

	public void setTile(int tile) {
		dirty = true;
		this.mTile = tile;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setState(IGameObjectState newState) {
		if (currentState != null)
			currentState.onLeave(this);

		currentState = newState;

		if (currentState != null)
			currentState.onEnter(this);
	}

	@Override
	public void setAnimation(int animation) {
		super.setAnimation(animation);
		super.setAnimation(animation, animationDuration());
	}

	public abstract float animationDuration();

	public void disableAI(float seconds) {
		checkAI = false;
		delayAI = seconds;
	}

	/**
	 * Call it when GameObject should be removed from game
	 */
	@Override
	public final void remove() {
		super.remove();
		onRemove();
		notifier.notifyRemoved(this);
		notifier.removeAll();

		// Send to pool
		GameWorld.$().recycle(this);
	}

	@Override
	public final void update(float delta) {
		if (!isOutOfGame()) {
			super.update(delta);
			if (!checkAI) {
				delayAI -= delta;
				if (delayAI <= 0.0f) {
					checkAI = true;
					delayAI = 0.0f;
				}
			}
			onUpdate(delta);

			if (isAlive()) {
				// AI don't need to be checked on every cycle
				if (checkAI) {
					disableAI(0.5f);
					currentState.onUpdate(this);
				}
			} else {

				if (animation != ANIMATION_DIE) {
					setAnimation(ANIMATION_DIE);
					GameWorld.$().getNotifier().onGameObjectStartDie(this);
				}

				if (isAnimationEnd())
					remove();
			}
		}
	}

	/**
	 * Override, called when GameObject is removed from game
	 */
	abstract void onRemove();

	/**
	 * Override, called when GameObject is updated. It will not be called while
	 * GameObject is out of game.
	 * 
	 * @param delta
	 *            Seconds elapsed until last cycle
	 */
	abstract void onUpdate(float delta);

	// ========================================================================
	// AI Methods
	// ========================================================================

	public boolean startState(IGameObjectState newState) {
		return startState(newState, null, null);
	}

	public boolean startState(IGameObjectState newState,
			GameObjectOwned newTarget) {
		return startState(newState, newTarget, null);
	}

	public boolean startState(IGameObjectState newState,
			GameObjectOwned newTarget, Object data) {
		target = newTarget;
		this.dataState = data;
		if (newState.start(this)) {
			setState(newState);
			return true;
		} else {
			target = null;
			this.dataState = null;
			return false;
		}
	}

	public boolean canSeeEnemy() {
		return false;
	}

	public void delayUpdate(float seconds) {
		// Delay AI updates
		checkAI = false;
		delayAI = seconds;
	}

	public boolean isAdjacent(GameObjectOwned target2) {
		int delta = Math.abs(getTile() - target2.getTile());
		return (delta == 1)
				|| (delta == GameWorld.$().getMap().getTiled().width);
	}

	public boolean canSenseNoise() {
		return false;
	}

	public int getNoisedTile() {
		return -1;
	}
}
