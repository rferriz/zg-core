package com.zombiegame.logic.gameobject;

public class GameObjectTask {

	// ========================================================================
	// Constants
	// ========================================================================

	// Basic tasks
	public static final int INVALID = -1;
	public static final int IDLE = INVALID + 1;
	public static final int MOVE_TO = IDLE + 1;

	// Survivor only tasks
	public static final int MOVE_AND_LOAD = MOVE_TO + 1;
	public static final int MOVE_AND_UNLOAD = MOVE_AND_LOAD + 1;

	// ========================================================================
	// Fields
	// ========================================================================

	int task;
	GameObjectOwned target;

	// ========================================================================
	// Constructor
	// ========================================================================

	public GameObjectTask() {
		reset();
	}

	// ========================================================================
	// Methods
	// ========================================================================

	public void reset() {
		task = 0;
		target = null;
	}

	public void set(int task, GameObjectOwned target) {
		this.task = task;
		this.target = target;
	}

	public void set(GameObjectTask next) {
		task = next.task;
		target = next.target;
	}

}
