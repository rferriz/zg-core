package com.zombiegame.logic.gameobject;

public class GameObjectSpawner extends GameObject {
	/* Time to close for alone survivor, a minute! */
	private static final float WorkNeeded = 60 * 1000f;

	/* Stores accumulated work */
	private float work;

	public GameObjectSpawner() {
		super();
		work = 0;
	}

	/**
	 * Building call this method to accumulate work before destroy it
	 * 
	 * @param building
	 */
	public void doWork(GameObjectBuilding building, float timeElapsed) {
		/* Accumulated work basically increase working survivors * timeElapsed */
		final int survivorCount = building.getInsideSurvivorsCount();
		final float totalGain = survivorCount * timeElapsed;

		work += totalGain;
		if (work >= WorkNeeded) {
			building.remove();
			remove();
			/* TODO Destroy this object and destroy building */
		}
	}
}
