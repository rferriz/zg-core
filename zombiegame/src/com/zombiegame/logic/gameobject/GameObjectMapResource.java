package com.zombiegame.logic.gameobject;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Ware;

public class GameObjectMapResource extends GameObjectOwned {

	int[] data;

	public GameObjectMapResource() {
		init();
	}

	public void init() {
		data = new int[Ware.Size];
	}

	public int resourceCount() {
		int count = 0;

		for (int i = 0; i < Ware.Size; i++)
			count += data[i];

		return count;
	}

	public void setResource(int key, int count) {
		data[key] = count;
	}

	public int resourceCount(int key) {
		return data[key];
	}

	public int randomResource(GameWorld world) {
		int result = Ware.none;

		if (resourceCount() < 1)
			return result;

		do {
			result = world.getRandom().nextInt(Ware.Size);
		} while (resourceCount(result) == 0);

		data[result]--;

		return result;
	}

	public boolean extractResource(int key) {
		if (data[key] > 0) {
			data[key]--;
			return true;
		}
		return false;
	}

	public boolean hasZombies() {
		return data[Ware.none] > 0;
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {
		// Nothing to do here
	}

	@Override
	void onRemove() {
		data = null;
	}

	@Override
	void onUpdate(float delta) {
		// Nothing to do here
	}

	@Override
	public void setTile(int tile) {
		super.setTile(tile);
		int mapy = GameWorld.$().tileToMapCoordY(tile);
		int mapx = GameWorld.$().tileToMapCoordX(tile, mapy);
		setPosition(mapx * GameWorld.$().getMap().getTiled().tileWidth,
				(GameWorld.$().getMap().getTiled().height - mapy - 1)
						* GameWorld.$().getMap().getTiled().tileHeight);

	}

	@Override
	public float animationDuration() {
		return 1f;
	}

}
