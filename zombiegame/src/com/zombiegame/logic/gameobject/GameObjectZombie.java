package com.zombiegame.logic.gameobject;

import java.util.List;

import com.badlogic.gdx.math.MathUtils;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.ai.AI_States;
import com.zombiegame.util.Assets;

public class GameObjectZombie extends GameObjectMover {

	private float realRangeView;
	private boolean readyToAttack;
	private float nextAttack;

	private int noiseTile;
	private float noiseTime;
	private float noisePower;

	public GameObjectZombie() {
		super();
		setName("zombie");
		realRangeView = properties().getRangeOfView()
				* GameWorld.$().getMap().getTiled().tileWidth;
		noiseTile = -1;
	}

	@Override
	protected boolean canWalkInto(int x, int y) {

		/* Zombies can not walk directly on buildings */
		if (GameWorld.$().getMap().getBuilding(x, y) != null)
			return false;

		/* Zombies can not walk directly on survivors */
		List<GameObjectOwned> gom = GameWorld.$().getMap().getUnits(x, y);
		for (int i = 0; i < gom.size(); i++) {
			if (gom.get(i) instanceof GameObjectSurvivor)
				return false;
		}

		return super.canWalkInto(x, y);
	}

	@Override
	public void reset() {
		super.reset();
		noiseTile = -1;
		currentState = AI_States.zIdle;
	}

	@Override
	public float animationDuration() {
		switch (animation) {
		case ANIMATION_ATTACK:
			return Assets.zombieSet.duration_attack;
		case ANIMATION_DIE:
			return Assets.zombieSet.duration_die;
		case ANIMATION_IDLE:
			return Assets.zombieSet.duration_idle;
		case ANIMATION_MOVE:
			return Assets.zombieSet.duration_walk;
		default:
			break;
		}
		return 1f;
	}

	private void checkForTarget() {
		if (target != null)
			return;

		checkForSurvivor();

		if (target == null)
			checkForBuilding();

		if (target != null)
			target.getNotifier().addListener(this);

	}

	private void checkForSurvivor() {
		List<GameObjectSurvivor> enemies = GameWorld.$().getSurvivors();
		for (int i = 0; i < enemies.size(); i++) {
			GameObjectSurvivor s = enemies.get(i);
			if (distance(s) < realRangeView) {
				target = s;
				return;
			}
		}
	}

	private void checkForBuilding() {
		List<GameObjectBuilding> places = GameWorld.$().getBuildings();
		for (int i = 0; i < places.size(); i++) {
			GameObjectBuilding b = places.get(i);
			if (distance(b) < realRangeView) {
				target = b;
				return;
			}
		}
	}

	@Override
	public void onUpdate(float delta) {
		if (!readyToAttack) {
			nextAttack -= delta;
			if (nextAttack < 0f)
				readyToAttack = true;
		}

		if (noiseTile > -1) {
			noiseTime -= delta;
			if (noiseTime < 0f) {
				noiseTile = -1;
				noisePower = 0f;
			}
		}
		super.onUpdate(delta);
	}

	/**
	 * For zombies, only target possible is Survivor or Building. So if Zombie
	 * has target, it can 'sense' enemy.
	 */
	@Override
	public boolean canSeeEnemy() {
		checkForTarget();
		return target != null;
	}

	public boolean readyToAttack() {
		if (isWalking())
			return false;
		return readyToAttack;
	}

	public void doAttack() {
		assert target != null;

		readyToAttack = false;
		setAnimation(ANIMATION_ATTACK);

		/* Face attacked */
		float dx = target.getPosition().x - position.x;
		float dy = target.getPosition().y - position.y;
		rotation = MathUtils.atan2(dy, dx) - MathUtils.PI * 0.5f;

		nextAttack = properties().getShotDelay();
		disableAI(animationDuration());
		target.hurt();
	}

	public void hearNoise(GameObjectOwned source, float power) {
		float realPower = (power * GameWorld.$().getMap().getTiled().tileWidth)
				/ distance(source);

		if (realPower > 1) {

			if (noiseTile < 0 || realPower > noisePower) {
				noiseTile = source.getTile();
				noiseTime = 2f;
				noisePower = realPower;
			}
		}
	}

	@Override
	public boolean canSenseNoise() {
		return (noiseTile >= 0);
	}

	@Override
	public int getNoisedTile() {
		return noiseTile;
	}
}
