package com.zombiegame.logic.gameobject;

import ai.GridPathfinder.GridMover;
import ai.GridPathfinder.GridNode;
import ai.GridPathfinder.GridPath;
import ai.GridPathfinder.GridPath.Step;
import ai.GridPathfinder.GridPathFinder;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.zombiegame.logic.GameWorld;

public abstract class GameObjectMover extends GameObjectOwned implements
		GridMover {

	// ========================================================================
	// Constants
	// ========================================================================

	private final static String TAG = "GameObjectMover";

	// ========================================================================
	// Fields
	// ========================================================================

	GridPathFinder pathFinder;
	GridPath path;
	boolean walking;
	int currentStep;

	Vector2 start, end;
	GridNode endNode;

	private int tileToFree;

	// ========================================================================
	// Constructor
	// ========================================================================

	public GameObjectMover() {
		super();
		GameObjectBobInit();
	}

	private void GameObjectBobInit() {
		pathFinder = null;
		start = new Vector2();
		end = new Vector2();
		tileToFree = -1;
	}

	@Override
	public void reset() {
		endNode = null;
		path = null;
		walking = false;
		super.reset();
		tileToFree = -1;
	}

	@Override
	public void dispose() {
		reset();
		pathFinder = null;
		endNode = null;
	}

	public void setPathfinder(GridPathFinder pathfinder) {
		this.pathFinder = pathfinder;
	}

	@Override
	public void getEndNode(GridNode node) {
		node = endNode;
	}

	public boolean walkToTile(int tile) {
		assert pathFinder != null;

		if (pathFinder == null)
			throw new RuntimeException(TAG + ": No pathfinder assigned");

		// Already walking
		if (path != null)
			return false;

		int sx, sy, tx, ty;
		sy = GameWorld.$().tileToMapCoordY(getTile());
		sx = GameWorld.$().tileToMapCoordX(getTile(), sy);

		ty = GameWorld.$().tileToMapCoordY(tile);
		tx = GameWorld.$().tileToMapCoordX(tile, ty);

		GridNode startNode = GameWorld.$().getMap().getNode(sx, sy);
		endNode = GameWorld.$().getMap().getNode(tx, ty);

		path = pathFinder.findPath(this, startNode, endNode);

		currentStep = 1;
		// Starting walk goes on update method
		return path != null;
	}

	public boolean isWalking() {
		return walking;
	}

	protected void cancelWalk() {
		if (path != null) {
			path = null;
		}
	}

	@Override
	void onRemove() {
		path = null;
		walking = false;
		currentStep = 0;
	}

	@Override
	public void onUpdate(float delta) {
		if (isAlive()) {
			moveBob(delta);
		} else if (animation != ANIMATION_DIE) {
			free(tileToFree);
			free(getTile());
			tileToFree = -1;
		}
	}

	public void free(int t) {
		if (t < 0)
			return;
		int x, y;
		y = GameWorld.$().tileToMapCoordY(t);
		x = GameWorld.$().tileToMapCoordX(t, y);
		GameWorld.$().getNotifier().onGameObjectLeaveMapPosition(this, x, y);
	}

	private void moveBob(float delta) {

		/* Clean up path when end position achieved */
		if (/* path != null && */position.x == end.x && position.y == end.y) {
			walking = false;
			end.set(0f, 0f);

			free(tileToFree);

			if (path != null) {
				// Stop when route completed
				currentStep++;
				if (currentStep >= path.getLength())
					path = null;
			}
		}

		/* If next step, process it */
		if (path != null) {
			if (!walking) {
				start.set(position);
				start.add(5, 5);
				tileToFree = getTile();
				Step step = path.getStep(currentStep);
				if (canWalkInto(step.getX(), step.getY())) {
					walking = true;
					end.set(step.getX(),
							(GameWorld.$().getMap().getTiled().height - 1)
									- step.getY());
					end.mul(GameWorld.$().getMap().getTiled().tileWidth);
					setStateWalking();
					GameWorld
							.$()
							.getNotifier()
							.onGameObjectReserveMapPosition(this, step.getX(),
									step.getY());
				} else {
					/* TODO Stop walking animation */

					cancelWalk();
				}
			}
		}

		/* Do walking */
		if (walking) {
			// Move
			float dx = end.x - position.x;
			float dy = end.y - position.y;

			dx = Math.abs(dx) > 2 ? Math.signum(dx) : 0f;
			dy = Math.abs(dy) > 2 ? Math.signum(dy) : 0f;

			dx *= delta * properties().getMovementSpeed();
			dy *= delta * properties().getMovementSpeed();

			// int newState = state;
			// if (newState != state)
			// setState(newState);

			if (dx == 0f)
				dx = end.x - position.x;
			if (dy == 0f)
				dy = end.y - position.y;

			setPosition(position.x + dx, position.y + dy);
		}
	}

	protected boolean canWalkInto(int x, int y) {
		return true;
	}

	private void setStateWalking() {
		float dx = end.x - start.x;
		float dy = end.y - start.y;
		rotation = MathUtils.atan2(dy, dx) - MathUtils.PI * 0.5f;
		if (animation != ANIMATION_MOVE)
			setAnimation(ANIMATION_MOVE);
	}

	@Override
	public void notifyRemoved(GameObjectOwned object) {
		if (target == object)
			cancelWalk();

		super.notifyRemoved(object);
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {
	}

}
