package com.zombiegame.logic.gameobject;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.WareStorage;
import com.zombiegame.logic.ai.AI_States;
import com.zombiegame.util.Assets;

public class GameObjectSurvivor extends GameObjectMover {

	// ========================================================================
	// Constants
	// ========================================================================

	private static final String TAG = "Survivor";

	// ========================================================================
	// Fields
	// ========================================================================

	WareStorage mWare;
	GameObjectBuilding home;

	// ========================================================================
	// Constructor
	// ========================================================================

	public GameObjectSurvivor() {
		super();
		SurvivorInit();
	}

	private void SurvivorInit() {
		mWare = new WareStorage(Ware.none, 0);
		setName("survivor");
		currentState = AI_States.Idle;
	}

	@Override
	protected boolean canWalkInto(int x, int y) {

		/* Survivors can not walk on zombies */
		List<GameObjectOwned> gom = GameWorld.$().getMap().getUnits(x, y);
		for (int i = 0; i < gom.size(); i++) {
			if (gom.get(i) instanceof GameObjectZombie)
				return false;
		}

		return super.canWalkInto(x, y);
	}

	@Override
	public void reset() {
		mWare.key = Ware.none;
		mWare.value = 0;
		setHome(null);
		super.reset();
		currentState = AI_States.Idle;
	}

	@Override
	public float animationDuration() {
		switch (animation) {
		case ANIMATION_ATTACK:
			return Assets.survivorSet.duration_attack;
		case ANIMATION_DIE:
			return Assets.survivorSet.duration_die;
		case ANIMATION_IDLE:
			return Assets.survivorSet.duration_idle;
		case ANIMATION_MOVE:
			return Assets.survivorSet.duration_walk;
		default:
			break;
		}
		return 1f;
	}

	public WareStorage getWare() {
		return mWare;
	}

	public boolean hasGoods() {
		return (mWare.value != 0);
	}

	public void setHome(GameObjectBuilding building) {
		if (home == building)
			return;

		if (home != null)
			home.unregisterSurvivor(this);

		home = building;

		if (home != null)
			home.registerSurvivor(this);
	}

	public GameObjectBuilding getHome() {
		return home;
	}

	public void setWare(int none, int quantity) {
		mWare.key = none;
		mWare.value = quantity;
	}

	@Override
	void onRemove() {
		// Unlink from house
		setHome(null);

		// TODO Drop resources to map

		// Clean ware
		mWare.key = Ware.none;
		mWare.value = 0;

		super.onRemove();
	}

	public void unloadGoods() {
		unloadGoods(target);
	}

	private void unloadGoods(GameObjectOwned building) {
		if (building == null)
			throw new IllegalArgumentException(
					"unloadGoods: Target cannot be null");

		if (getTile() != building.getTile()) {
			/**
			 * FIXME: This may happen when zombie is stopped by zombies while
			 * carrying goods
			 */
			Gdx.app.log(TAG, "unloadGoods: Target not on same tile as survivor");
			return;
		}

		if (building instanceof GameObjectBuilding) {
			GameObjectBuilding b = (GameObjectBuilding) building;
			b.getStorage().carrierArrived(this);
		}
	}

	public void loadGoods() {
		loadGoods(target);
	}

	private void loadGoods(GameObjectOwned building) {

		if (building == null)
			throw new IllegalArgumentException(
					"loadGoods: Target cannot be null");

		if (getTile() != building.getTile())
			throw new RuntimeException(
					"loadGoods: Target not on same tile as survivor");

		if (building instanceof GameObjectBuilding) {

			GameObjectBuilding b = (GameObjectBuilding) building;
			int stored = b.getStorage().getStored(mWare.key);

			if (stored > Ware.pkgSize(mWare.key))
				stored = Ware.pkgSize(mWare.key);

			b.getStorage().decreaseStored(mWare.key, stored);
			setWare(getWare().key, stored);
		} else {
			throw new IllegalArgumentException(
					"target must be instanceof Building");
		}

	}

	public void findHome() {
		if (home != null)
			return;

		final List<GameObjectBuilding> buildings = GameWorld.$().getBuildings();

		for (int index = 0; index < buildings.size(); index++) {
			GameObjectBuilding b = buildings.get(index);
			if ((!b.isOutOfGame())
					&& b.getName().equals(GameObjectBuilding.WAREHOUSE)
					&& distance(b) < GameWorld.$().getMap().getTiled().tileWidth * 15) {
				Gdx.app.log(TAG, "New home found");
				setHome(b);
				return;
			}
		}
	}

	@Override
	public void notifyRemoved(GameObjectOwned object) {
		super.notifyRemoved(object);
		if (home == object) {
			setHome(null);
		}
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {
		super.notifyUpdated(object);
	}

	public boolean hasHome() {
		return home != null;
	}

	public void notifyOneLessInHome() {
		setHome(null);
	}

}
