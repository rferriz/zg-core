package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class SurvivorReturnState extends DummyState {

	@Override
	public boolean start(GameObjectOwned go) {
		boolean result = false;

		GameObjectSurvivor s = (GameObjectSurvivor) go;
		if (s.hasHome())
			result = s.walkToTile(s.getHome().getTile());

		return result;
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {
		GameObjectSurvivor s = (GameObjectSurvivor) go;

		if (s.canSeeEnemy()) {
			s.startState(AI_States.RunAway);
		} else if (!s.hasHome()) {
			s.startState(AI_States.Idle);
		} else if (!s.isWalking()) {
			s.startState(AI_States.Idle);
		}

	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
	}

}
