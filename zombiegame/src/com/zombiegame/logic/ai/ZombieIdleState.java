package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public class ZombieIdleState extends DummyState {

	@Override
	public boolean start(GameObjectOwned go) {
		go.setAnimation(GameObjectOwned.ANIMATION_IDLE);
		return super.start(go);
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
		go.target = null;
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {

		if (go.canSeeEnemy()) {
			go.startState(AI_States.zAttack, go.target);
		} else if (go.canSenseNoise()) {
			go.startState(AI_States.zAlert, null, go.getNoisedTile());
		} else {
			/* Zombies are specially slow reacting */
			go.delayUpdate(1f);
		}
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
		super.onMessage(go, msg);
	}

}
