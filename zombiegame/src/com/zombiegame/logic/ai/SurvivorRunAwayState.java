package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

public class SurvivorRunAwayState extends DummyState {

	@Override
	public boolean start(GameObjectOwned go) {
		return super.start(go);
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
		GameObjectSurvivor s = (GameObjectSurvivor) go;
		s.notifyOneLessInHome();
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {
		if (!go.canSeeEnemy())
			go.setState(AI_States.Idle);
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
	}

}
