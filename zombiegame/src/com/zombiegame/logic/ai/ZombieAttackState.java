package com.zombiegame.logic.ai;

import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectZombie;

public class ZombieAttackState extends DummyState {

	@Override
	public boolean start(GameObjectOwned go) {
		if (!go.hasTarget())
			throw new RuntimeException("ZombieAttackState needs a target");

		return super.start(go);
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		super.onEnter(go);
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		go.setAnimation(GameObjectOwned.ANIMATION_IDLE);
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {
		super.onUpdate(go);

		if (!go.hasTarget()) {
			go.startState(AI_States.zIdle);
		} else {
			GameObjectZombie z = (GameObjectZombie) go;
			if (go.isAdjacent(go.target)) {
				if (z.readyToAttack()) {
					z.doAttack();
				}
			} else {
				if (!z.isWalking())
					z.walkToTile(z.target.getTile());
			}
		}
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
		super.onMessage(go, msg);
	}

}
