package com.zombiegame.logic.ai;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectZombie;

/**
 * AlertState. On this state, bob is walking to a suspicious position. If enemy
 * is seen, go and attack it. If destination reached, just wait here.
 * Destination position (tile) should be stored on dataState
 * 
 * @author raul
 * 
 */
public class ZombieAlertState extends DummyState {

	private static final String TAG = "ZombieAlertState";

	@Override
	public boolean start(GameObjectOwned go) {
		if (go.dataState == null)
			throw new RuntimeException(
					"Cannot start ZombieAlertState without dataState");

		GameObjectZombie z = (GameObjectZombie) go;
		return z.walkToTile((Integer) go.dataState);
	}

	@Override
	public void onEnter(GameObjectOwned go) {
		Gdx.app.log(TAG, "onEnter");
		super.onEnter(go);
	}

	@Override
	public void onLeave(GameObjectOwned go) {
		Gdx.app.log(TAG, "onLeave");
		super.onLeave(go);
	}

	@Override
	public void onUpdate(GameObjectOwned go) {
		super.onUpdate(go);

		if (go.canSeeEnemy()) {
			go.startState(AI_States.zAttack, go.target);
		} else {
			GameObjectZombie z = (GameObjectZombie) go;
			if (!z.isWalking())
				go.startState(AI_States.zIdle);
		}
	}

	@Override
	public void onMessage(GameObjectOwned go, GameObjectMessage msg) {
		// TODO Auto-generated method stub
		super.onMessage(go, msg);
	}

}
