package com.zombiegame.logic.ai;

public class AI_States {
	/* Survivor states */
	public static final IGameObjectState Idle = new SurvivorIdleState();
	public static final IGameObjectState Return = new SurvivorReturnState();
	public static final IGameObjectState RunAway = new SurvivorRunAwayState();
	public static final IGameObjectState Transport = new SurvivorTransportState();
	public static final IGameObjectState Retrieve = new SurvivorRetrieveState();

	/* Zombie states */
	public static final IGameObjectState zIdle = new ZombieIdleState();
	public static final IGameObjectState zAlert = new ZombieAlertState();
	public static final IGameObjectState zAttack = new ZombieAttackState();
}