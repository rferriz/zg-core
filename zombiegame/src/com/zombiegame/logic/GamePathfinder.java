package com.zombiegame.logic;

import javax.management.RuntimeErrorException;

import ai.GridPathfinder.GridAStarPathfinder;
import ai.GridPathfinder.GridPathFinder;
import ai.GridPathfinder.GridHeuristics.GridHeuristicEuclidean;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.events.WorldListener;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectOwned;

public class GamePathfinder implements WorldListener {

	private static final String TAG = "GamePathFinder";

	GridPathFinder pathfinder;

	public GamePathfinder() {
	}

	@Override
	public void onMapLoaded() {
		long start, end;

		start = System.currentTimeMillis();

		// Create pathfinder
		pathfinder = new GridAStarPathfinder(GameWorld.$().getMap(),
				new GridHeuristicEuclidean());

		end = System.currentTimeMillis();

		Gdx.app.log(TAG, "Mapfinder initialized in " + (end - start) + "mS");
	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
		if (object instanceof GameObjectBuilding) {
			GameObjectBuilding building = (GameObjectBuilding) object;
			int x, y;
			y = GameWorld.$().tileToMapCoordY(building.getTile());
			x = GameWorld.$().tileToMapCoordX(building.getTile(), y);

			if (GameWorld.$().getMap().getBuilding(x, y) != null)
				throw new RuntimeErrorException(new Error(TAG),
						"Already a building on " + x + ", " + y);

			GameWorld.$().getMap().setBuilding(building, x, y);
		} else {
			int x, y;
			y = GameWorld.$().tileToMapCoordY(object.getTile());
			x = GameWorld.$().tileToMapCoordX(object.getTile(), y);
			GameWorld.$().getMap().addUnit(object, x, y);
		}
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		if (object instanceof GameObjectBuilding) {
			GameObjectBuilding building = (GameObjectBuilding) object;
			int x, y;
			y = GameWorld.$().tileToMapCoordY(building.getTile());
			x = GameWorld.$().tileToMapCoordX(building.getTile(), y);

			if (GameWorld.$().getMap().getBuilding(x, y) != building)
				throw new RuntimeErrorException(new Error(TAG),
						"Building not at " + x + ", " + y);

			GameWorld.$().getMap().setBuilding(null, x, y);
		} else {

			int x, y;
			y = GameWorld.$().tileToMapCoordY(object.getTile());
			x = GameWorld.$().tileToMapCoordX(object.getTile(), y);
			GameWorld.$().getMap().removeUnit(object, x, y);

			for (int i = 0; i < GameWorld.$().getMap().getTiled().width; i++) {
				for (int j = 0; j < GameWorld.$().getMap().getTiled().height; j++) {
					GameWorld.$().getMap().removeUnit(object, i, j);
				}
			}
		}
	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned go) {
	}

	public GridPathFinder getPathfinder() {
		return pathfinder;
	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
		GameWorld.$().getMap().addUnit(go, col, row);
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
		GameWorld.$().getMap().removeUnit(go, col, row);
	}

	@Override
	public void onPaused() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onResumed() {
		// TODO Auto-generated method stub
	}

}
