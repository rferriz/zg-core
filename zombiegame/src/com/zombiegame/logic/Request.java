package com.zombiegame.logic;

import com.badlogic.gdx.Gdx;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectSurvivor;

/*
 * Request is used by two modes as Request and as Supply.
 * 
 * A Request is linked with an mSource (the Consumer) and has info about ware
 * type requested, quantity, priorities, time between priorities updates and 
 * let mCarrier to null. Later, when an EconomyNodeWorker is assigned to 
 * supply this request, then mCarrier is set to Survivor.
 * 
 * A Supply is a request on Producer's side.
 */
public class Request implements Comparable<Request> {

	// ========================================================================
	// Constants
	// ========================================================================
	private static final String TAG = "Request";

	// ========================================================================
	// Fields
	// ========================================================================

	WareStorage mWare;
	int mPriorityFixed;
	int mPriorityDynamical;
	GameObjectBuilding mSource;
	GameObjectSurvivor mCarrier;

	// ========================================================================
	// Methods
	// ========================================================================

	public Request() {
		mWare = new WareStorage(Ware.none, 0);
		reset();
	}

	public void reset() {
		Gdx.app.log(TAG, "Reset!");
		setSource(null);
		setCarrier(null);
		mWare.key = Ware.none;
		mWare.value = 0;
		mPriorityFixed = 0;
		mPriorityDynamical = 0;
	}

	public void initialize(final int key, final int pQuantity) {
		mWare.key = key;
		mWare.value = pQuantity;
	}

	// ========================================================================
	// Getter & setter methods
	// ========================================================================

	public GameObjectBuilding getSource() {
		return mSource;
	}

	public void setSource(GameObjectBuilding pSource) {

		if (mSource == pSource)
			return;

		Gdx.app.log(TAG, "Seting request source to " + pSource);

		if (mSource != null)
			mSource.removeRequest(this);

		mSource = pSource;

		if (mSource != null)
			mSource.addRequest(this);

	}

	public GameObjectSurvivor getCarrier() {
		return mCarrier;
	}

	public void setCarrier(final GameObjectSurvivor pCarrier) {

		if (mCarrier == pCarrier)
			return;

		mCarrier = pCarrier;

	}

	public WareStorage getWare() {
		return mWare;
	}

	public void updatePriority() {
		if (mSource != null) {
			mPriorityFixed = mSource.getPriority(mWare.key);
		}
	}

	public int getPrority() {
		return mPriorityFixed;
	}

	/*
	 * Compare of request are always related with priority
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Request another) {
		return (mPriorityFixed + mPriorityDynamical)
				- (another.mPriorityFixed + another.mPriorityDynamical);
	}

	@Override
	public String toString() {
		return "Owner: " + (mSource == null ? "" : mSource) + " Ware: ["
				+ Ware.name[mWare.key] + ", " + mWare.value + "]";
	}
}
