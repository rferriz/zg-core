package com.zombiegame.logic.events;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector3;

public class UINotifier implements UIListener {

	ArrayList<UIListener> listeners;

	public UINotifier() {
		listeners = new ArrayList<UIListener>();
	}

	public void addListener(UIListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(UIListener listener) {
		listeners.remove(listener);
	}

	public void removeAllListeners() {
		listeners.clear();
	}

	@Override
	public void tap(Vector3 position) {
		for (int i = 0; i < listeners.size(); i++) {
			final UIListener listener = listeners.get(i);
			listener.tap(position);
		}
	}

	@Override
	public void doubleTap(Vector3 position) {
		for (int i = 0; i < listeners.size(); i++) {
			final UIListener listener = listeners.get(i);
			listener.doubleTap(position);
		}
	}

	@Override
	public void holdTap(Vector3 position) {
		for (int i = 0; i < listeners.size(); i++) {
			final UIListener listener = listeners.get(i);
			listener.holdTap(position);
		}
	}

}
