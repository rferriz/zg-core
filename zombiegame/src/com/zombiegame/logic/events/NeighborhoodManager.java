package com.zombiegame.logic.events;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectOwned;

public class NeighborhoodManager implements WorldListener {

	@Override
	public void onMapLoaded() {
		// Nothing to do
	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
		if (object instanceof GameObjectBuilding) {
			final GameObjectBuilding b = (GameObjectBuilding) object;

			for (int index = 0; index < GameWorld.$().getBuildings().size(); index++) {
				final GameObjectBuilding n = GameWorld.$().getBuildings()
						.get(index);

				// Prevents self neighborhood
				if (b == n)
					continue;

				if (b.inRange(n, false)) {
					n.addNeighbor(b);
					b.addNeighbor(n);
				}
			}

			GameWorld.$().getEconomyManager().addNode(b);
		}
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		if (object instanceof GameObjectBuilding) {
			final GameObjectBuilding b = (GameObjectBuilding) object;
			while (b.getNeighborhood().size() > 0) {
				final GameObjectBuilding other = b.getNeighborhood().get(
						b.getNeighborhood().size() - 1);
				b.removeNeighbor(other);
			}

			GameWorld.$().getEconomyManager().removeNode(b);
		}
	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
		// Nothing to do
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
		// Nothing to do
	}

	@Override
	public void onPaused() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onResumed() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned object) {
		// TODO Auto-generated method stub
	}

}
