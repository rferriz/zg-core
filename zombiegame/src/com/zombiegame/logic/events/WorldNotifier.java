package com.zombiegame.logic.events;

import com.badlogic.gdx.utils.Array;
import com.zombiegame.logic.gameobject.GameObjectOwned;

public class WorldNotifier implements WorldListener {

	private final Array<WorldListener> listeners;

	public WorldNotifier() {
		listeners = new Array<WorldListener>();
	}

	public void addListener(WorldListener listener) {
		listeners.add(listener);
	}

	@Override
	public void onMapLoaded() {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onMapLoaded();
		}
	}

	@Override
	public void onGameObjectPlaced(GameObjectOwned object) {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onGameObjectPlaced(object);
		}
	}

	@Override
	public void onGameObjectRemoved(GameObjectOwned object) {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onGameObjectRemoved(object);
		}
	}

	@Override
	public void onGameObjectStartDie(GameObjectOwned object) {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onGameObjectStartDie(object);
		}
	}

	@Override
	public void onGameObjectReserveMapPosition(GameObjectOwned go, int col,
			int row) {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onGameObjectReserveMapPosition(go, col, row);
		}
	}

	@Override
	public void onGameObjectLeaveMapPosition(GameObjectOwned go, int col,
			int row) {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onGameObjectLeaveMapPosition(go, col, row);
		}
	}

	@Override
	public void onPaused() {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onPaused();
		}
	}

	@Override
	public void onResumed() {
		for (int index = 0; index < listeners.size; index++) {
			final WorldListener listener = listeners.get(index);
			listener.onResumed();
		}
	}

}
