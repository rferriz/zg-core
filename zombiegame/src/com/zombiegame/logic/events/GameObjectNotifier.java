package com.zombiegame.logic.events;

import java.util.ArrayList;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public class GameObjectNotifier implements GameObjectListener {

	private ArrayList<GameObjectListener> listeners;

	public GameObjectNotifier() {
		listeners = new ArrayList<GameObjectListener>();
	}

	public void addListener(GameObjectListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(GameObjectListener listener) {
		listeners.remove(listener);
	}

	public void removeAll() {
		listeners.clear();
	}

	@Override
	public void notifyRemoved(GameObjectOwned object) {
		for (int index = listeners.size() - 1; index >= 0; index--) {
			listeners.get(index).notifyRemoved(object);
		}
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {
		for (int index = listeners.size() - 1; index >= 0; index--) {
			listeners.get(index).notifyUpdated(object);
		}
	}
}
