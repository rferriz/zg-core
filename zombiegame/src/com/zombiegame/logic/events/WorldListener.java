package com.zombiegame.logic.events;

import com.zombiegame.logic.gameobject.GameObjectOwned;

public interface WorldListener {

	void onMapLoaded();

	void onGameObjectPlaced(GameObjectOwned object);

	void onGameObjectRemoved(GameObjectOwned object);

	void onGameObjectStartDie(GameObjectOwned object);

	void onGameObjectReserveMapPosition(GameObjectOwned go, int col, int row);

	void onGameObjectLeaveMapPosition(GameObjectOwned go, int col, int row);

	void onPaused();

	void onResumed();
}
