package com.zombiegame.logic.events;

import com.badlogic.gdx.math.Vector3;

public interface UIListener {
	public void tap(Vector3 position);

	public void doubleTap(Vector3 position);

	public void holdTap(Vector3 position);
}
