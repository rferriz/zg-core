package com.zombiegame.logic.modules;

import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.logic.gameobject.GameObjectZombie;
import com.zombiegame.util.Assets;

public class Shooter extends BuildingModule {

	// ========================================================================
	// Constants
	// ========================================================================

	// private static final String TAG = "ShooterBuilding";

	// ========================================================================
	// Fields
	// ========================================================================

	protected boolean mShotReady;
	protected float mShotDelay;
	protected float mShotHitChance;
	protected GameObjectOwned mTarget;

	// ========================================================================
	// Methods
	// ========================================================================

	public Shooter(GameObjectBuilding pBuilding) {
		super(pBuilding);
		mShotReady = true;
		mShotDelay = 0.0f;
		mShotHitChance = 0.5f;
	}

	// ========================================================================
	// Override
	// ========================================================================

	@Override
	protected void onDestroy() {
	}

	@Override
	protected void onClean() {
		mTarget = null;
	}

	@Override
	protected void onWork(float pSecondsElapsed) {

		if (mShotReady) {
			final boolean hasSurvivors = mBuilding.getInsideSurvivorsCount() > 0;
			final boolean hasAmmo = hasAmmo();

			if (hasSurvivors && hasAmmo) {

				if (searchForTarget()) {

					synchronized (mTarget) {
						if (mTarget.isAlive()) {

							int ammoId = Ware.valueOf(mBuilding.properties()
									.getShotAmmo());

							// Check for no ammo consume
							if (ammoId != Ware.none)
								mBuilding.getStorage()
										.decreaseStored(ammoId, 1);
							shootTarget(mTarget);
							mShotReady = false;
							mShotDelay = mBuilding.properties().getShotDelay();
						}
					}
				}
			}
		} else {
			mShotDelay -= pSecondsElapsed;
			if (mShotDelay < 0.0f) {
				mShotReady = true;
			}
		}

	}

	// ========================================================================
	// Helper methods
	// ========================================================================

	private boolean hasAmmo() {
		int ammoId = Ware.valueOf(mBuilding.properties().getShotAmmo());
		if (ammoId == Ware.none)
			return true;
		return mBuilding.getStorage().getStored(ammoId) > 0;
	}

	private void shootTarget(final GameObjectOwned pTarget) {

		final float cHit = GameWorld.$().getRandom().nextFloat();

		Assets.shotgun_fire.play();

		mShotHitChance = 0.5f;
		// mShotHitChance = 0.2f + mBuilding.getSleepingSurvivorsCount() * 0.1f;

		GameWorld.$().makeNoise(mBuilding,
				mBuilding.properties().getShotNoise());

		if (cHit < mShotHitChance) {
			mTarget.hurt();
			if (!mTarget.isAlive()) {
				mTarget = null;
			}
		}
	}

	private boolean searchForTarget() {

		if (mTarget != null) {
			if (!GameWorld.$().getZombies().contains(mTarget))
				mTarget = null;
		}

		// Search for zombies!
		if (mTarget == null || !mTarget.isAlive()) {
			mTarget = null;
			float fDistance;
			float fNearestDistance = mBuilding.properties().getShotRange()
					* GameWorld.$().getMap().getTiled().tileWidth;

			for (int i = 0; i < GameWorld.$().getZombies().size(); i++) {
				final GameObjectZombie zZombie = GameWorld.$().getZombies()
						.get(i);
				if (zZombie.isAlive()) {
					fDistance = mBuilding.distance(zZombie);

					if (fDistance < fNearestDistance) {
						// TODO Check visibility
						fNearestDistance = fDistance;
						mTarget = zZombie;
					}
				}
			}
		}
		return (mTarget != null);
	}

}
