package com.zombiegame.logic.economy;

import java.util.ArrayList;

public class EconomyManager {

	ArrayList<GameEconomy> economies;
	ArrayList<GameEconomy> tmp;

	float accumulated;

	public EconomyManager() {
		economies = new ArrayList<GameEconomy>();
		tmp = new ArrayList<GameEconomy>();
	}

	public void addNode(EconomyNode node) {
		tmp.clear();
		GameEconomy economy;

		for (int i = 0; i < economies.size(); i++) {

			economy = economies.get(i);

			if (economy.inRange(node)) {
				economy.addEconomyNode(node);
				tmp.add(economy);
			}
		}

		if (tmp.size() == 0) {
			economy = new GameEconomy();
			economy.addEconomyNode(node);
			economies.add(economy);
		}

		if (tmp.size() > 1)
			mergeEconomies(tmp);

	}

	public void removeNode(EconomyNode node) {

		/* Acquire economy of node */
		GameEconomy ge = getEconomyOf(node);

		/* Remove economy from economies list */
		economies.remove(ge);

		/* Create economy from non removed nodes */
		for (int i = 0; i < ge.nodes.size(); i++) {
			/* Ignore node being removed */
			if (ge.nodes.get(i) == node)
				continue;

			addNode(ge.nodes.get(i));
		}
	}

	public void mergeEconomies(ArrayList<GameEconomy> economy) {
		GameEconomy e;

		e = economy.get(0);

		for (int i = 1; i < economy.size(); i++) {
			GameEconomy e1 = economy.get(i);
			economies.remove(e1);
			for (int j = 0; j < e1.nodes.size(); j++) {
				e.addEconomyNode(e1.nodes.get(j));
			}
			e1.nodes.clear();
		}
	}

	public GameEconomy getEconomyOf(EconomyNode node) {

		for (int i = 0; i < economies.size(); i++) {

			GameEconomy ge = economies.get(i);

			if (ge.nodes.contains(node))
				return ge;

		}

		return null;
	}

	public void compute(final float delta) {
		accumulated += delta;
		if (accumulated < 30)
			return;
		accumulated = 0;
		for (int i = 0; i < economies.size(); i++) {
			economies.get(i).updatePriorities();
		}

	}

}
