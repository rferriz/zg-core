package com.zombiegame.logic.economy;

public interface EconomyNode {

	public int getStock(int ware);

	public void setBalanced(int ware, int quantity);

	public boolean inRange(EconomyNode node);
}
