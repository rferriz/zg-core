package com.zombiegame.logic;

public class WareStorage {
	public int key;
	public int value;

	public WareStorage(int key, int value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o instanceof WareStorage) {
			WareStorage tStorage = (WareStorage) o;
			return key == tStorage.key && (this.value == tStorage.value);
		}
		return false;

	}

}
