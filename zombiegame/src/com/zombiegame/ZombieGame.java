package com.zombiegame;

import javax.management.RuntimeErrorException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.zombiegame.screens.GameScreen;
import com.zombiegame.screens.MainMenuScreen;
import com.zombiegame.screens.OptionsScreen;
import com.zombiegame.screens.SplashScreen;
import com.zombiegame.util.GameReporter;

public class ZombieGame extends Game {
	public static final int WIDTH = 854;
	public static final int HEIGHT = 480;

	SplashScreen mSplashScreen;
	MainMenuScreen mMainMenuScreen;
	OptionsScreen moOptionsScreen;
	GameScreen mGameScreen;
	public GameReporter gameReporter;

	private static ZombieGame instance;
	Preferences config;

	public ZombieGame(GameReporter reporter) {
		super();
		gameReporter = reporter;
		if (instance != null)
			throw new RuntimeErrorException(new Error("Initialization"),
					"Another instance is running");
		instance = this;
	}

	public static ZombieGame $() {
		if (instance == null)
			throw new RuntimeErrorException(new Error("Instance Access"),
					"There are no instance created");
		return instance;
	}

	public Preferences config() {
		if (config == null)
			config = Gdx.app.getPreferences(".ZombieGame.cfg");
		return $().config;
	}

	@Override
	public void create() {

		// Create screen
		mSplashScreen = new SplashScreen(this);
		mMainMenuScreen = new MainMenuScreen(this);
		moOptionsScreen = new OptionsScreen();
		// mGameScreen = new GameScreen(this);

		setScreen(mSplashScreen);
	}

	public void showMenu() {
		if (mMainMenuScreen != null) {
			setScreen(mMainMenuScreen);
		} else {
			showGame();
		}
	}

	public void showGame() {
		if (mGameScreen != null) {
			setScreen(mGameScreen);
		}
	}

	public void showOptions() {
		if (moOptionsScreen != null) {
			setScreen(moOptionsScreen);
		}
	}

	/* Strings for Settings */
	public static final String MusicVolume = "MfxVolume";
	public static final String SfxVolume = "SfxVolume";

}
