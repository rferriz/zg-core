package com.zombiegame.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.TileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.zombiegame.logic.gameobject.GameObjectBuilding;

public class Assets {
	public static AnimationSet zombieSet;
	public static AnimationSet survivorSet;
	public static Map<Integer, Animation> buildingSet;
	public static Map<Integer, Sprite> constructionSet;

	public static ArrayList<TextureRegion> scrap;

	public static TextureRegion mainBackgound;
	public static TextureRegion optionsBackgound;

	private static TextureAtlas mTextureAtlas;
	private static TileAtlas mMapAtlas;
	public static FileHandle baseDir;
	public static FileHandle mapBaseDir;

	public static BitmapFont font;
	public static BitmapFont menuFont;

	public static Skin skin;

	public static boolean loading = false;
	public static boolean loaded = false;
	private static TextureAtlas menuTextureAtlas;
	public static TextureRegion zombieMarker;
	public static Sprite ellipse;

	/* Music assets */
	static public Music musicMenu;
	static public Music musicGame;

	/* Sound assets */
	static public Sound click;
	static public Sound shotgun_fire;

	public static TileAtlas getMapAtlas() throws Exception {
		if (mMapAtlas == null)
			throw new Exception(
					"Can't get map atlas because it is not generated");
		return mMapAtlas;
	}

	public static void loadMenu() {
		menuTextureAtlas = new TextureAtlas(
				Gdx.files.internal("data/gfx/menu/pack"));

		mainBackgound = menuTextureAtlas.findRegion("backmain");
		optionsBackgound = menuTextureAtlas.findRegion("zwp");

		musicMenu = Gdx.audio.newMusic(Gdx.files
				.internal("data/music/spotted.ogg"));
		click = Gdx.audio.newSound(Gdx.files
				.internal("data/sfx/Shotgun-reload.ogg"));

		font = new BitmapFont();
		menuFont = new BitmapFont(Gdx.files.internal("data/menuFont.fnt"),
				false);

		skin = new Skin(Gdx.files.internal("data/uiskin.json"),
				Gdx.files.internal("data/uiskin.png"));

	}

	public static void load() {
		baseDir = Gdx.files.internal("data");
		mapBaseDir = Gdx.files.internal("data/maps");

		musicGame = Gdx.audio.newMusic(Gdx.files
				.internal("data/music/zombie_main_music.ogg"));

		mTextureAtlas = new TextureAtlas(
				Gdx.files.internal("data/gfx/game/pack"));
		mMapAtlas = null;

		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				long start, end;

				start = System.currentTimeMillis();
				Assets.loading = true;
				Assets.AsyncLoadSurvivorSet();
				Assets.AsyncLoadZombieSet();
				Assets.AsyncLoadBuildingSet();
				Assets.AsyncLoadConstructrionSet();
				ellipse = new Sprite(mTextureAtlas.findRegion("ellipse"));
				shotgun_fire = Gdx.audio.newSound(Gdx.files
						.internal("data/sfx/Shotgun-fire.ogg"));
				end = System.currentTimeMillis();

				Assets.loading = false;
				Assets.loaded = true;
				Gdx.app.log("ASSETS", "Loaded async in " + (end - start) + "ms");
			}
		});
		th.start();
		Gdx.app.log("ASSETS", "EndLoad");
	}

	private static void AsyncLoadBuildingSet() {
		buildingSet = new HashMap<Integer, Animation>();

		buildingSet.put(GameObjectBuilding.WAREHOUSE.hashCode(), new Animation(
				1f, mTextureAtlas.findRegion("tornfaseHouse")));

		buildingSet.put(GameObjectBuilding.GATHERHOUSE.hashCode(),
				new Animation(1f, mTextureAtlas.findRegion("factorySimple")));

		buildingSet.put(GameObjectBuilding.GUNTOWER.hashCode(), new Animation(
				1f, mTextureAtlas.findRegion("outpost")));

		scrap = new ArrayList<TextureRegion>();
		scrap.add(mTextureAtlas.findRegion("scrap1"));
		scrap.add(mTextureAtlas.findRegion("scrap2"));
		scrap.add(mTextureAtlas.findRegion("scrap3"));

		zombieMarker = mTextureAtlas.findRegion("zombie-spawn");
	}

	private static void AsyncLoadConstructrionSet() {
		constructionSet = new HashMap<Integer, Sprite>();

		Sprite tmp;

		tmp = new Sprite(mTextureAtlas.findRegion("tornfaseHouse"));
		constructionSet.put(GameObjectBuilding.WAREHOUSE.hashCode(), tmp);

		tmp = new Sprite(mTextureAtlas.findRegion("factorySimple"));
		constructionSet.put(GameObjectBuilding.GATHERHOUSE.hashCode(), tmp);

		tmp = new Sprite(mTextureAtlas.findRegion("outpost"));
		constructionSet.put("outpost".hashCode(), tmp);
	}

	public static void loadMapAtlas(TiledMap pMap) {
		mMapAtlas = new TileAtlas(pMap, mapBaseDir);
	}

	private static void AsyncLoadSurvivorSet() {

		List<AtlasRegion> tempList = new ArrayList<TextureAtlas.AtlasRegion>();

		survivorSet = new AnimationSet();

		tempList.add(mTextureAtlas.findRegion("survivor-00"));
		survivorSet.attack = new Animation(0.25f, tempList);
		survivorSet.duration_attack = 0.25f * tempList.size();

		survivorSet.die = new Animation(0.25f, tempList);
		survivorSet.duration_die = 0.25f * tempList.size();

		survivorSet.idle = new Animation(0.25f, tempList);
		tempList.clear();
		tempList.add(mTextureAtlas.findRegion("survivor-00"));
		tempList.add(mTextureAtlas.findRegion("survivor-01"));
		tempList.add(mTextureAtlas.findRegion("survivor-02"));
		tempList.add(mTextureAtlas.findRegion("survivor-03"));
		tempList.add(mTextureAtlas.findRegion("survivor-04"));
		tempList.add(mTextureAtlas.findRegion("survivor-05"));
		tempList.add(mTextureAtlas.findRegion("survivor-06"));
		tempList.add(mTextureAtlas.findRegion("survivor-07"));
		tempList.add(mTextureAtlas.findRegion("survivor-08"));
		tempList.add(mTextureAtlas.findRegion("survivor-09"));
		tempList.add(mTextureAtlas.findRegion("survivor-10"));
		tempList.add(mTextureAtlas.findRegion("survivor-11"));
		tempList.add(mTextureAtlas.findRegion("survivor-12"));
		tempList.add(mTextureAtlas.findRegion("survivor-13"));
		tempList.add(mTextureAtlas.findRegion("survivor-14"));
		tempList.add(mTextureAtlas.findRegion("survivor-15"));

		survivorSet.run = new Animation(0.05f, tempList);
		survivorSet.duration_run = 0.05f * tempList.size();

		survivorSet.walk = new Animation(0.1f, tempList);
		survivorSet.duration_walk = 0.1f * tempList.size();
	}

	private static void AsyncLoadZombieSet() {
		List<AtlasRegion> tmpList = new ArrayList<AtlasRegion>();
		zombieSet = new AnimationSet();

		zombieSet = new AnimationSet();
		tmpList = new ArrayList<TextureAtlas.AtlasRegion>();
		tmpList.add(mTextureAtlas.findRegion("zombie-00"));
		tmpList.add(mTextureAtlas.findRegion("zombie-01"));
		tmpList.add(mTextureAtlas.findRegion("zombie-02"));
		tmpList.add(mTextureAtlas.findRegion("zombie-03"));
		tmpList.add(mTextureAtlas.findRegion("zombie-02"));
		tmpList.add(mTextureAtlas.findRegion("zombie-01"));
		zombieSet.idle = new Animation(0.3f, tmpList);
		zombieSet.duration_idle = 0.3f * tmpList.size();

		tmpList = new ArrayList<TextureAtlas.AtlasRegion>();
		tmpList.add(mTextureAtlas.findRegion("zombie-05"));
		tmpList.add(mTextureAtlas.findRegion("zombie-06"));
		tmpList.add(mTextureAtlas.findRegion("zombie-07"));
		tmpList.add(mTextureAtlas.findRegion("zombie-08"));
		tmpList.add(mTextureAtlas.findRegion("zombie-09"));
		tmpList.add(mTextureAtlas.findRegion("zombie-10"));
		tmpList.add(mTextureAtlas.findRegion("zombie-11"));
		tmpList.add(mTextureAtlas.findRegion("zombie-04"));
		zombieSet.walk = new Animation(0.3f, tmpList);
		zombieSet.duration_walk = 0.3f * tmpList.size();

		zombieSet.run = zombieSet.walk;
		zombieSet.duration_run = 0.3f * tmpList.size();

		tmpList = new ArrayList<TextureAtlas.AtlasRegion>();
		tmpList.add(mTextureAtlas.findRegion("zombie-12"));
		tmpList.add(mTextureAtlas.findRegion("zombie-13"));
		tmpList.add(mTextureAtlas.findRegion("zombie-14"));
		tmpList.add(mTextureAtlas.findRegion("zombie-15"));
		tmpList.add(mTextureAtlas.findRegion("zombie-16"));
		tmpList.add(mTextureAtlas.findRegion("zombie-17"));
		tmpList.add(mTextureAtlas.findRegion("zombie-20"));
		zombieSet.attack = new Animation(0.25f, tmpList);
		zombieSet.duration_attack = 0.25f * tmpList.size();

		tmpList = new ArrayList<TextureAtlas.AtlasRegion>();
		tmpList.add(mTextureAtlas.findRegion("zombie-21"));
		tmpList.add(mTextureAtlas.findRegion("zombie-22"));
		tmpList.add(mTextureAtlas.findRegion("zombie-23"));
		tmpList.add(mTextureAtlas.findRegion("zombie-24"));
		tmpList.add(mTextureAtlas.findRegion("zombie-25"));
		tmpList.add(mTextureAtlas.findRegion("zombie-26"));
		tmpList.add(mTextureAtlas.findRegion("zombie-27"));
		zombieSet.die = new Animation(0.2f, tmpList);
		zombieSet.duration_die = 0.2f * tmpList.size();

	}

}
