package com.zombiegame.util;

import com.badlogic.gdx.Gdx;

public class DummyGameReporter implements GameReporter {

	@Override
	public void sendReport(Throwable throwable) {
		// Dummy for desktop
		Gdx.app.log("SendReport", throwable.toString());
	}

	@Override
	public boolean isDummy() {
		return true;
	}

}
