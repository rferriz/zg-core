package com.zombiegame.util;

import com.badlogic.gdx.graphics.g2d.Animation;

public class AnimationSet {
	public Animation idle;
	public Animation walk;
	public Animation run;
	public Animation attack;
	public Animation die;

	public float duration_idle;
	public float duration_walk;
	public float duration_run;
	public float duration_attack;
	public float duration_die;
}