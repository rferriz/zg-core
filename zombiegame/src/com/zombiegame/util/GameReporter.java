package com.zombiegame.util;

public interface GameReporter {
	public boolean isDummy();

	public void sendReport(Throwable throwable);
}
