package com.zombiegame.util;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.zombiegame.logic.events.UINotifier;

public class OrthographicCameraController extends InputAdapter {

	public static final long HOLD_DELAY = 500;
	public static final long SINGLE_DELAY = 200;

	OrthographicCamera mCamera;

	final Vector2 maxCamPosition = new Vector2();
	final Vector3 newCamera = new Vector3();

	final Vector3 mCurr = new Vector3();
	final Vector3 mLast = new Vector3(-1, -1, 0);
	final Vector3 mDelta = new Vector3();

	final Vector3 mDown = new Vector3();
	final Vector3 mUp = new Vector3();
	final Vector3 mTapDown = new Vector3();
	final Vector3 mTapUp = new Vector3();

	long previousTap;
	long currentTap;
	long currentUp;

	ScheduledThreadPoolExecutor holdTimer;
	@SuppressWarnings("rawtypes")
	ScheduledFuture holdFuture;
	Runnable holdDetected;

	@SuppressWarnings("rawtypes")
	ScheduledFuture singleTapFuture;
	Runnable singleTapDetected;

	boolean isPressed;
	UINotifier notifier;

	public OrthographicCameraController(OrthographicCamera pCamera, int upLeft,
			int downRight) {
		this.mCamera = pCamera;
		previousTap = 0;
		holdTimer = new ScheduledThreadPoolExecutor(3);

		holdDetected = new Runnable() {
			@Override
			public void run() {
				OrthographicCameraController.this.holdTaped();
			}
		};

		singleTapDetected = new Runnable() {
			@Override
			public void run() {
				OrthographicCameraController.this.singleTap();
			}
		};

		maxCamPosition.set(upLeft, downRight);
		notifier = new UINotifier();
	}

	public UINotifier getNotifier() {
		return notifier;
	}

	@Override
	public boolean keyDown(int keycode) {
		return super.keyDown(keycode);
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return super.keyUp(keycode);
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		// Ignore multitouch
		if (pointer != 0)
			return false;

		mDown.set(x, y, 0);
		mCamera.unproject(mCurr.set(mDown));
		mTapDown.set(mCurr);
		currentTap = System.currentTimeMillis();
		holdFuture = holdTimer.schedule(holdDetected, HOLD_DELAY,
				TimeUnit.MILLISECONDS);
		isPressed = true;
		return super.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean touchDragged(int pX, int pY, int pPointer) {
		// Ignore multitouch
		if (pPointer != 0)
			return false;

		if (!isPressed)
			return false;

		mCamera.unproject(mCurr.set(pX, pY, 0));
		if (!(mLast.x == -1 && mLast.y == -1 && mLast.z == -1)) {
			mCamera.unproject(mDelta.set(mLast.x, mLast.y, 0));
			mDelta.sub(mCurr);

			newCamera.set(mCamera.position.x, mCamera.position.y, 0);
			newCamera.add(mDelta.x, mDelta.y, 0);

			if (newCamera.x < Gdx.graphics.getWidth() / 2)
				newCamera.x = Gdx.graphics.getWidth() / 2;

			if (newCamera.x > maxCamPosition.x - Gdx.graphics.getWidth() / 2)
				newCamera.x = maxCamPosition.x - Gdx.graphics.getWidth() / 2;

			if (newCamera.y < Gdx.graphics.getHeight() / 2)
				newCamera.y = Gdx.graphics.getHeight() / 2;

			if (newCamera.y > maxCamPosition.y - Gdx.graphics.getHeight() / 2)
				newCamera.y = maxCamPosition.y - Gdx.graphics.getHeight() / 2;

			mCamera.position.set(newCamera.x, newCamera.y, 0);
		}
		mLast.set(pX, pY, 0);

		// Movement disables long taps
		holdFuture.cancel(false);

		return false;
	}

	@Override
	public boolean touchUp(int pX, int pY, int pPointer, int pButton) {
		// Ignore multitouch
		if (pPointer != 0)
			return false;

		if (!isPressed)
			return false;

		holdFuture.cancel(false);

		mUp.set(pX, pY, 0);
		mCamera.unproject(mTapUp.set(pX, pY, 0));

		if (isDoubleTap()) {
			if (singleTapFuture != null)
				singleTapFuture.cancel(false);
			// Avoid multiple double tap
			previousTap = 0;
			doubleTapped();
		} else if (isSingleTap()) {
			singleTapFuture = holdTimer.schedule(singleTapDetected,
					SINGLE_DELAY, TimeUnit.MILLISECONDS);
		} else {
			currentTap = 0; // Prevents false double-tappping
		}
		previousTap = currentTap;

		currentUp = System.currentTimeMillis();
		mLast.set(-1, -1, -1);
		return false;
	}

	private boolean isDoubleTap() {
		// Short duration (1sec max)
		if (previousTap == 0)
			return false;

		if (currentTap - previousTap < 500) {
			// TODO Check for near position
			return true;
		}

		return false;
	}

	private boolean isSingleTap() {
		return ((Math.abs(mUp.x - mDown.x) < 10) && (Math.abs(mUp.y - mDown.y) < 10));
	}

	public void doubleTapped() {
		Gdx.app.log("Input", "Double tap detected");
		notifier.doubleTap(mTapUp);
	}

	public void holdTaped() {
		Gdx.app.log("Input", "Hold detected");
		// Do this to prevent launch a 'tap' event when user left his finger
		isPressed = false;
		// I should use tapDown because mTapUp is not updated
		notifier.holdTap(mTapDown);
	}

	public void singleTap() {
		/* Prevent false double taps */
		previousTap = 0;
		notifier.tap(mTapUp);
	}
}
