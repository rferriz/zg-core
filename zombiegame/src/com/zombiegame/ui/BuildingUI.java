package com.zombiegame.ui;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ClickListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.badlogic.gdx.utils.Disposable;
import com.zombiegame.logic.GameWorld;
import com.zombiegame.logic.Ware;
import com.zombiegame.logic.WorldView;
import com.zombiegame.logic.events.GameObjectListener;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.gameobject.GameObjectOwned;
import com.zombiegame.util.Assets;

public class BuildingUI implements GameObjectListener, InputProcessor,
		Disposable {

	// ========================================================================
	// Constants
	// ========================================================================

	// static private final String TAG = "BuildingUI";

	// ========================================================================
	// Fields
	// ========================================================================

	GameObjectBuilding attached;
	boolean focus;

	CustomWindow window;
	GameWorld world;
	WorldView view;
	Camera camera;

	TextButton button_close;
	TextButton button_destroy;

	Label woodLabel;
	Label woodQty;
	Button woodMore;
	Button woodLess;

	Label bulletLabel;
	Label bulletQty;
	Button bulletMore;
	Button bulletLess;

	Label survivorLabel;
	Label survivorQty;
	Button survivorMore;
	Button survivorLess;

	StringBuilder tmp;

	public BuildingUI(GameWorld world, WorldView worldView) {
		this.attached = null;
		this.camera = worldView.getCamera();
		this.world = world;
		this.view = worldView;
		this.window = null;

		tmp = new StringBuilder();

	}

	@Override
	public void dispose() {
		attached = null;
		world = null;
		view = null;
		camera = null;
		tmp = null;

		// Dispose ui
		window = null;
		button_close = null;
		button_destroy = null;

		bulletLabel = null;
		bulletLess = null;
		bulletMore = null;
		bulletQty = null;

		woodLabel = null;
		woodLess = null;
		woodMore = null;
		woodQty = null;

		survivorLabel = null;
		survivorLess = null;
		survivorMore = null;
		survivorQty = null;
	}

	public void register(GameObjectBuilding building) {
		if (window == null)
			setup();

		if (attached == building) {
			placeOnGameObject();
			return;
		}

		if (attached != null)
			attached.getNotifier().removeListener(this);

		attached = building;

		placeOnGameObject();
		if (attached != null) {
			view.addInputProcessor(this);
			attached.getNotifier().addListener(this);
			updateControls();
			notifyUpdated(attached);
		} else {
			view.removeInputProcessor(this);
		}
	}

	private void updateControls() {

		String[] allowed;

		allowed = attached.properties().getAllowedWareControl();

		survivorLess.visible = false;
		survivorMore.visible = false;
		woodLess.visible = false;
		woodMore.visible = false;
		bulletLess.visible = false;
		bulletMore.visible = false;

		for (int i = 0; i < allowed.length; i++) {
			if (allowed[i].equals(Ware.name[Ware.bullet])) {
				bulletLess.visible = true;
				bulletMore.visible = true;
			}
			if (allowed[i].equals(Ware.name[Ware.wood])) {
				woodLess.visible = true;
				woodMore.visible = true;
			}
			if (allowed[i].equals(Ware.name[Ware.survivor])) {
				survivorLess.visible = true;
				survivorMore.visible = true;
			}
		}
	}

	private void placeOnGameObject() {
		if (attached == null) {
			// Put it out of viewport
			window.x = -window.width;
			window.y = -window.height;
			window.visible = false;
		} else {
			window.x = attached.getPosition().x;
			window.y = attached.getPosition().y - 16 - window.height;
		}
	}

	public void setup() {
		window = new CustomWindow("Win", "Title", camera,
				Assets.skin.getStyle(WindowStyle.class), 240, 194);

		button_close = new TextButton("Close",
				Assets.skin.getStyle(TextButtonStyle.class));
		button_destroy = new TextButton("Destroy",
				Assets.skin.getStyle(TextButtonStyle.class));
		button_close.setText("Close");
		button_destroy.setText("Destroy");

		woodLess = new TextButton("-",
				Assets.skin.getStyle(TextButtonStyle.class));
		woodQty = new Label("WHAT", Assets.skin);
		woodLabel = new Label(Ware.name[Ware.wood], Assets.skin);
		woodMore = new TextButton("+",
				Assets.skin.getStyle(TextButtonStyle.class), "bMoreWood");

		bulletLabel = new Label(Ware.name[Ware.bullet], Assets.skin);
		bulletQty = new Label("0", Assets.skin);
		bulletMore = new TextButton("+",
				Assets.skin.getStyle(TextButtonStyle.class), "bMoreBullet");
		bulletLess = new TextButton("-",
				Assets.skin.getStyle(TextButtonStyle.class), "bLessBullet");

		survivorLabel = new Label(Ware.name[Ware.survivor], Assets.skin);
		survivorQty = new Label("0", Assets.skin);
		survivorMore = new TextButton("+",
				Assets.skin.getStyle(TextButtonStyle.class), "bMoresurvivor");
		survivorLess = new TextButton("-",
				Assets.skin.getStyle(TextButtonStyle.class), "bLesssurvivor");

		window.defaults().spaceBottom(5).expandX();
		window.row();
		window.add(survivorLabel);
		window.add(survivorLess);
		window.add(survivorQty);
		window.add(survivorMore);
		window.row();
		window.add(woodLabel);
		window.add(woodLess);
		window.add(woodQty);
		window.add(woodMore);
		window.row();
		window.add(bulletLabel);
		window.add(bulletLess);
		window.add(bulletQty);
		window.add(bulletMore);
		window.row();
		window.add(button_destroy).left().colspan(2).expandX();
		window.add(button_close).right().colspan(2).expandX();

		// Set up listeners
		button_close.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				setVisible(false);
				register(null);
			}
		});

		button_destroy.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				setVisible(false);
				if (attached != null) {
					attached.remove();
				}
			}
		});

		woodLess.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().decreaseDesiredGoods(Ware.wood);
			}
		});
		woodMore.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().increaseDesiredGoods(Ware.wood);
			}
		});

		bulletLess.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().decreaseDesiredGoods(Ware.bullet);
			}
		});

		bulletMore.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().increaseDesiredGoods(Ware.bullet);
			}
		});

		survivorLess.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().decreaseDesiredGoods(Ware.survivor);
			}
		});

		survivorMore.setClickListener(new ClickListener() {
			@Override
			public void click(Actor actor, float x, float y) {
				if (attached != null)
					attached.getStorage().increaseDesiredGoods(Ware.survivor);
			}
		});

	}

	public void setVisible(boolean visible) {

		placeOnGameObject();

		// If nothing attached, ensures completly invisibility
		if (attached == null) {
			window.visible = false;
			view.removeInputProcessor(this);
			return;
		}

		if (visible == window.visible)
			return;

		window.visible = visible;
		// We have a building attached
		if (visible) {
			attached.getNotifier().addListener(this);
			view.addInputProcessor(this);
		} else {
			attached.getNotifier().removeListener(this);
			view.removeInputProcessor(this);
		}
	}

	public void render(SpriteBatch batch) {
		if (window != null)
			if (window.visible) {
				window.draw(batch, 1.0f);
			}
	}

	@Override
	public void notifyRemoved(GameObjectOwned object) {
		if (attached == null)
			return;

		if (attached != object)
			new RuntimeException(
					"Received notifyRemoved from non targeted building");

		attached = null;

		// Hide window
		setVisible(false);
	}

	@Override
	public void notifyUpdated(GameObjectOwned object) {

		if (attached != object)
			return;

		window.setTitle(attached.getName());

		tmp.setLength(0);
		tmp.append(attached.getStorage().getStored(Ware.wood)).append("/")
				.append(attached.getStorage().getDesired(Ware.wood));
		woodQty.setText(tmp.toString());
		woodQty.invalidate();
		woodQty.layout();

		tmp.setLength(0);
		tmp.append(attached.getStorage().getStored(Ware.bullet)).append("/")
				.append(attached.getStorage().getDesired(Ware.bullet));
		bulletQty.setText(tmp.toString());
		bulletQty.invalidate();
		bulletQty.layout();

		tmp.setLength(0);
		tmp.append(attached.getStorage().getStored(Ware.survivor)).append("/")
				.append(attached.getStorage().getDesired(Ware.survivor));
		survivorQty.setText(tmp.toString());
		survivorQty.invalidate();
		survivorQty.layout();

		window.invalidate();
		window.layout();
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	final Vector3 vec = new Vector3();

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		view.getCamera().unproject(vec.set(x, y, 0));
		vec.sub(window.x, window.y, 0);

		Actor hitted = window.hit(vec.x, vec.y);
		if (hitted == null)
			return false;

		boolean result = hitted.touchDown(vec.x, vec.y, pointer);

		focus = result;

		return result;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		view.getCamera().unproject(vec.set(x, y, 0));
		vec.sub(window.x, window.y, 0);

		Actor hitted = null;

		if (window.focusedActor[0] != null) {
			for (int i = window.getActors().size() - 1; i >= 0; i--) {
				Actor a = window.getActors().get(i);
				hitted = a.hit(vec.x - a.x, vec.y - a.y);
				if (hitted != null)
					break;
			}
		}

		if (hitted == null)
			hitted = window.hit(vec.x, vec.y);

		for (int i = 0; i < window.getActors().size(); i++) {
			Actor a = window.getActors().get(i);
			a.touchUp(vec.x - a.x, vec.y - a.y, pointer);
		}

		window.touchUp(vec.x, vec.y, pointer);
		window.focus(null, pointer);

		focus = false;
		return (hitted != null);
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		view.getCamera().unproject(vec.set(x, y, 0));
		vec.sub(window.x, window.y, 0);

		if (focus) {
			Actor hitted = window.hit(vec.x, vec.y);
			if (hitted == null)
				hitted = window;
			hitted.touchDragged(vec.x, vec.y, pointer);
			return true;
		}
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		view.getCamera().unproject(vec.set(x, y, 0));
		vec.sub(window.x, window.y, 0);

		Actor hitted = window.hit(vec.x, vec.y);
		if (hitted == null)
			return false;

		hitted.touchMoved(vec.x, vec.y);
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isVisible() {
		return window.visible;
	}

	public GameObjectBuilding getBuilding() {
		return attached;
	}

}
