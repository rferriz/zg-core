package com.zombiegame.ui;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.zombiegame.logic.WorldView;
import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.util.Assets;

public class ConstructionUI implements InputProcessor, Disposable {

	WorldView view;
	boolean visible;
	Vector3 root;
	ArrayList<Sprite> sprites;

	public ConstructionUI(WorldView view) {
		this.view = view;
		root = new Vector3();
		sprites = new ArrayList<Sprite>();
		setup();
	}

	public void setup(float x, float y) {
		root.set(x, y, 0);
		for (int i = 0; i < sprites.size(); i++) {
			Sprite s = sprites.get(i);
			Vector3 tmp = calculatePosition(root.x, root.y, i);
			s.setPosition(tmp.x - s.getWidth() * 0.5f, tmp.y - s.getHeight()
					* 0.5f);
		}
	}

	public Vector3 calculatePosition(float centerx, float centery, int num) {
		double ang = num * 360 / sprites.size() + 90;
		Vector3 t = new Vector3();
		int d = view.getWorld().getMap().getTiled().tileWidth;
		t.x = (float) (centerx + d * Math.sin(Math.toRadians(ang)));
		t.y = (float) (centery + d * Math.cos(Math.toRadians(ang)));
		t.z = 0;
		return t;
	}

	private void setup() {
		Sprite tmp;

		tmp = new Sprite(
				Assets.constructionSet.get(GameObjectBuilding.WAREHOUSE
						.hashCode()));
		tmp.scale(-0.5f);
		sprites.add(tmp);

		tmp = new Sprite(
				Assets.constructionSet.get(GameObjectBuilding.GATHERHOUSE
						.hashCode()));
		tmp.scale(-0.5f);
		sprites.add(tmp);

		tmp = new Sprite(Assets.constructionSet.get("outpost".hashCode()));
		tmp.scale(-0.5f);
		sprites.add(tmp);
	}

	@Override
	public void dispose() {
		sprites.clear();
		sprites = null;
		root = null;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		// TODO Tween all sprites from root.x root.y to its final position

		if (visible) {
			view.addInputProcessor(this);
		} else {
			view.removeInputProcessor(this);
		}
	}

	public void draw(SpriteBatch batch) {
		if (!visible)
			return;

		for (int i = 0; i < sprites.size(); i++) {
			final Sprite s = sprites.get(i);
			s.draw(batch);
		}
	}

	static private boolean spriteHit(Sprite sprite, float x, float y) {
		return sprite.getBoundingRectangle().contains(x, y);
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	Vector3 vec = new Vector3();

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		view.getCamera().unproject(vec.set(x, y, 0));

		for (int i = 0; i < sprites.size(); i++) {
			Sprite spr = sprites.get(i);
			if (spriteHit(spr, vec.x, vec.y)) {
				Gdx.app.log("CUI", "Srpite pressed " + i);
				view.buildAt(i, root);
				setVisible(false);
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchMoved(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
