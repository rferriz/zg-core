package com.zombiegame.pools;

import com.zombiegame.logic.gameobject.GameObjectSurvivor;
import com.zombiegame.logic.util.GenericPool;

public class SurvivorPool extends GenericPool<GameObjectSurvivor> {

	@Override
	protected GameObjectSurvivor onAllocatePoolItem() {
		return new GameObjectSurvivor();
	}

	@Override
	protected void onHandleRecycleItem(final GameObjectSurvivor pSurvivor) {
		pSurvivor.reset();
	}

	@Override
	protected void onHandleObtainItem(final GameObjectSurvivor pSurvivor) {
		pSurvivor.reset();
	}

}
