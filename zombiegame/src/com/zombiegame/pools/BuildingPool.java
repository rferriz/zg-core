package com.zombiegame.pools;

import com.zombiegame.logic.gameobject.GameObjectBuilding;
import com.zombiegame.logic.util.GenericPool;

public class BuildingPool extends GenericPool<GameObjectBuilding> {

	@Override
	protected GameObjectBuilding onAllocatePoolItem() {
		return new GameObjectBuilding();
	}

	@Override
	protected void onHandleRecycleItem(final GameObjectBuilding pBuilding) {
		pBuilding.reset();
	}

	@Override
	protected void onHandleObtainItem(final GameObjectBuilding pBuilding) {
		pBuilding.reset();
	}

}
