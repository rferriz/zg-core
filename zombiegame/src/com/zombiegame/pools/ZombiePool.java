package com.zombiegame.pools;

import com.zombiegame.logic.gameobject.GameObjectZombie;
import com.zombiegame.logic.util.GenericPool;

public class ZombiePool extends GenericPool<GameObjectZombie> {

	@Override
	protected GameObjectZombie onAllocatePoolItem() {
		return new GameObjectZombie();
	}

	@Override
	protected void onHandleRecycleItem(final GameObjectZombie pZombie) {
		pZombie.reset();
	}

	@Override
	protected void onHandleObtainItem(final GameObjectZombie pZombie) {
		pZombie.reset();
	}

}
