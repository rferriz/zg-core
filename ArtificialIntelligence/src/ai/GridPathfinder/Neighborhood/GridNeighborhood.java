package ai.GridPathfinder.Neighborhood;

import ai.GridPathfinder.GridMap;
import ai.GridPathfinder.GridMover;
import ai.GridPathfinder.GridNode;
import ai.GridPathfinder.GridPathFinder;

public abstract class GridNeighborhood {
    private int count;
    protected GridNode[] neighborhood;

    public GridNeighborhood() {
	this(8);
    }

    public GridNeighborhood(int maxNeighbors) {
	count = 0;
	neighborhood = new GridNode[maxNeighbors];
    }

    public void reset() {
	count = 0;
    }

    public int size() {
	return count;
    }

    protected void addNeighbor(final GridNode node) {
	if (count == neighborhood.length)
	    throw new RuntimeException(
		    "Not enought space to store more neighbors");
	neighborhood[count] = node;
	count++;
    }

    public abstract GridNode[] findNeighborhood(final GridMap map,
	    final GridPathFinder finder, final GridMover mover,
	    final GridNode node);

}
