package ai.GridPathfinder.Neighborhood;

import ai.GridPathfinder.GridMap;
import ai.GridPathfinder.GridMover;
import ai.GridPathfinder.GridNode;
import ai.GridPathfinder.GridPathFinder;

public class GridSquaredDiagonalNeighborhood extends GridNeighborhood {

    private enum Diagonal {
	NorthWest, NorthEast, SouthEast, SouthWest
    }

    @Override
    public GridNode[] findNeighborhood(final GridMap map,
	    final GridPathFinder finder, final GridMover mover,
	    final GridNode node) {
	reset();
	int x, y;

	// NorthWest
	x = node.getX() - 1;
	y = node.getY() - 1;
	if (finder.validNode(mover, x, y)
		&& validDiagonal(mover, finder, node, Diagonal.NorthWest))
	    addNeighbor(map.getNode(x, y));

	// North
	x++;
	if (finder.validNode(mover, x, y))
	    addNeighbor(map.getNode(x, y));

	// NortEast
	x++;
	if (finder.validNode(mover, x, y)
		&& validDiagonal(mover, finder, node, Diagonal.NorthEast))
	    addNeighbor(map.getNode(x, y));

	// East
	y++;
	if (finder.validNode(mover, x, y))
	    addNeighbor(map.getNode(x, y));

	// SouthEast
	y++;
	if (finder.validNode(mover, x, y)
		&& validDiagonal(mover, finder, node, Diagonal.SouthEast))
	    addNeighbor(map.getNode(x, y));

	// South
	x--;
	if (finder.validNode(mover, x, y))
	    addNeighbor(map.getNode(x, y));

	// SouthWest
	x--;
	if (finder.validNode(mover, x, y)
		&& validDiagonal(mover, finder, node, Diagonal.NorthWest))
	    addNeighbor(map.getNode(x, y));

	// West
	y--;
	if (finder.validNode(mover, x, y))
	    addNeighbor(map.getNode(x, y));

	return neighborhood;
    }

    private boolean validDiagonal(GridMover mover, GridPathFinder finder,
	    GridNode node, Diagonal direction) {
	boolean allowed = false;

	switch (direction) {
	case NorthEast:
	    allowed = finder.validNode(mover, node.getX(), node.getY() - 1)
		    && finder.validNode(mover, node.getX() + 1, node.getY());
	    break;
	case NorthWest:
	    allowed = finder.validNode(mover, node.getX(), node.getY() - 1)
		    && finder.validNode(mover, node.getX() - 1, node.getY());
	    break;
	case SouthEast:
	    allowed = finder.validNode(mover, node.getX(), node.getY() + 1)
		    && finder.validNode(mover, node.getX() + 1, node.getY());
	    break;
	case SouthWest:
	    allowed = finder.validNode(mover, node.getX(), node.getY() - 1)
		    && finder.validNode(mover, node.getX() - 1, node.getY());
	    break;
	}

	return allowed;
    }
}
