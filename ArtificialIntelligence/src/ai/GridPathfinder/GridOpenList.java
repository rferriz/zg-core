package ai.GridPathfinder;

import java.util.ArrayList;

public class GridOpenList {
    private ArrayList<GridNode> list;
    private int indexBest;

    public GridOpenList() {
	list = new ArrayList<GridNode>();
    }

    public boolean remove(GridNode node) {
	throw new RuntimeException("Not implemented");
    }

    public boolean contains(GridNode node) {
	boolean found = false;
	for (int i = 0; i < list.size(); i++) {
	    GridNode tmp = list.get(i);
	    if ((node.x == tmp.x) && (node.y == tmp.y)) {
		found = true;
		break;
	    }
	}

	return found;
    }

    public void add(GridNode node) {
	list.add(node);
    }

    public int size() {
	return list.size();
    }

    public void clear() {
	list.clear();
    }

    public GridNode findBest() {
	indexBest = 0;
	float bestCost = Float.MAX_VALUE;

	for (int i = 0; i < list.size(); i++) {
	    GridNode n = list.get(i);
	    if (n.f < bestCost) {
		indexBest = i;
		bestCost = n.f;
	    }
	}

	return list.remove(indexBest);
    }
}
