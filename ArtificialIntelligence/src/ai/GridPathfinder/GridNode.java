package ai.GridPathfinder;

public class GridNode {
    int x;
    int y;
    float f; // Walked + Heuristic
    float g; // Cost from start point (walked)
    float h; // Cost to goal (heuristic)
    public GridNode parent;

    public GridNode() {
	this(0, 0, Float.MAX_VALUE, 0);
    }

    public GridNode(int x, int y) {
	this(x, y, 0, 0);
    }

    public GridNode(int x, int y, float g, float h) {
	this.x = x;
	this.y = y;
	this.g = g;
	this.h = h;
	this.f = 0;
	this.parent = null;
    }

    /**
     * Clear pathfinder garbage data
     */
    public void reset() {
	g = 0;
	h = 0;
	f = 0;
	parent = null;
    }

    public int getX() {
	return x;
    }

    public int getY() {
	return y;
    }

    public boolean equalLocation(Object other) {
	boolean equals = false;
	if (other instanceof GridNode) {
	    GridNode otherNode = (GridNode) other;
	    equals = (x == otherNode.x) && (y == otherNode.y);
	}
	return equals;
    }

    @Override
    public int hashCode() {
	return 4096 * x + y;
    }

}
