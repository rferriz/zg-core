package ai.GridPathfinder;

public abstract class GridPathFinder {

    public abstract GridPath findPath(GridMover mover, GridNode start,
	    GridNode end);

    public abstract boolean validNode(GridMover mover, int x, int y);
}
