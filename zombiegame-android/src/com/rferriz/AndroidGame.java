package com.rferriz;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.zombiegame.ZombieGame;

public class AndroidGame extends AndroidApplication {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(android.os.Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
	config.useGL20 = false;
	config.useAccelerometer = false;
	config.useCompass = false;
	initialize(new ZombieGame(new AcraGameReporter()), config);
    }
}