package com.rferriz;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpPostSender;

import android.app.Application;

// BugSenseURI = "http://www.bugsense.com/api/acra?api_key=96ae3ef6"
// GoogleDocFormKey = "dDY4dW1keHU3c2VQbU5jXzFNTF94eHc6MQ"
@ReportsCrashes(formKey = "dDY4dW1keHU3c2VQbU5jXzFNTF94eHc6MQ")
public class ZombieGameACRA extends Application {

    @Override
    public void onCreate() {
	ACRA.init((Application) this);
	ErrorReporter.getInstance().addReportSender(
		new HttpPostSender(
			"http://www.bugsense.com/api/acra?api_key=96ae3ef6",
			null));
	super.onCreate();
    }
}
