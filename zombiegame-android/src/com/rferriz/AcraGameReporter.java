package com.rferriz;

import org.acra.ErrorReporter;

import com.zombiegame.util.GameReporter;

public class AcraGameReporter implements GameReporter {

    @Override
    public void sendReport(Throwable throwable) {
	ErrorReporter.getInstance().handleSilentException(throwable);
    }

    @Override
    public boolean isDummy() {
	return false;
    }

}
